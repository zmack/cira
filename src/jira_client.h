//
// Created by Andrei Bocan on 10/6/20.
//

#ifndef FIRST_JIRA_CLIENT_H
#define FIRST_JIRA_CLIENT_H

#include <iostream>
#include <nlohmann/json.hpp>
#include <cpr/cpr.h>
#include <cpr/session.h>
#include <cpr/ssl_options.h>

namespace cira {
    class JiraClient {

        std::string authorizationKey;
        std::string domain;


    public:
        explicit JiraClient(std::string domain, std::string authorizationKey) : domain(std::move(domain)),
                                                                                authorizationKey(std::move(authorizationKey)) {};

        ~JiraClient() = default;

        void getProject();

        void getProjectStatus(const std::string &boardKey);

        void getIssue(const std::string &issueId);

    private:
        cpr::Response get(const cpr::Url &url);
    };

    class JiraUser {
        std::string displayName;
        std::string emailAddress;
        std::string accountId;

    public:
        [[nodiscard]] const std::string &getDisplayName() const {
            return displayName;
        }

        [[nodiscard]] const std::string &getEmailAddress() const {
            return emailAddress;
        }

        [[nodiscard]] const std::string &getAccountId() const {
            return accountId;
        }

    public:
        explicit JiraUser(const nlohmann::basic_json<> &payload) : displayName(payload["displayName"]),
                                                                   emailAddress(payload["emailAddress"]),
                                                                   accountId(payload["accountId"]) {};
    };

    class JiraTicket {
        std::string summary;
        JiraUser creator;

    public:
        [[nodiscard]] const std::string &getSummary() const {
            return summary;
        }

        [[nodiscard]] const JiraUser &getCreator() const {
            return creator;
        }


    public:
        explicit JiraTicket(const nlohmann::basic_json<> &payload) : summary(payload["fields"]["summary"]),
                                                                     creator(payload["fields"]["creator"]) {};
    };

}


#endif //FIRST_JIRA_CLIENT_H
