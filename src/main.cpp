#include <iostream>
#include <string>
#include <utility>

#include "jira_client.h"

using namespace cira;

int main() {
    std::string authorizationKey = std::getenv("JIRA_AUTHORIZATION_KEY");

    if (authorizationKey.empty()) {
        std::cout << "No authorization key set" << std::endl;
        exit(-1);
    }
    auto client = JiraClient{"https://team-1600408910060.atlassian.net",
                             authorizationKey};
//    client.getProject();
//    client.getProjectStatus("INC");
    client.getIssue("INC-1");
    return 0;
}