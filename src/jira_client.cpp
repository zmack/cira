//
// Created by Andrei Bocan on 10/6/20.
//

#include "jira_client.h"

namespace cira {
    void JiraClient::getProject() {
        auto fullUrl = domain + "/rest/api/3/project";

        cpr::Response r = get(cpr::Url{fullUrl});

        auto parsed_json = nlohmann::json::parse(r.text);
        std::cout << parsed_json << std::endl;
    }

    void JiraClient::getProjectStatus(const std::string &boardKey) {
        auto fullUrl = domain + "/rest/api/3/project/" + boardKey + "/statuses";

        cpr::Response r = get(cpr::Url{fullUrl});

        auto parsed_json = nlohmann::json::parse(r.text);
        std::cout << parsed_json << std::endl;
    }

    void JiraClient::getIssue(const std::string &issueId) {
        auto fullUrl = domain + "/rest/api/3/issue/" + issueId;

        cpr::Response r = get(cpr::Url{fullUrl});

        auto parsed_json = nlohmann::json::parse(r.text);
        std::cout << parsed_json << std::endl;
        auto ticket = JiraTicket{parsed_json};
        std::cout << ticket.getCreator().getDisplayName() << std::endl;
    }

    cpr::Response JiraClient::get(const cpr::Url &url) {
        auto verifySsl = cpr::VerifySsl{false};

        return cpr::Get(url,
                        cpr::Header{{"Authorization", "Basic " + this->authorizationKey}},
                        verifySsl);
    }
}